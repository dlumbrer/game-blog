---
title: "The legend of Zelda: Tears of the Kingdom"
date: 2023-06-04
tags: ["switch", "nintendo", "action", "adventure", "open-world"]
---

__Platform__: Nintendo Switch \
__Genre__: Action adventure


The masterpiece to put the cherry on top of this magnificent console, the Nintendo Switch.

![alt](https://zelda.com/tears-of-the-kingdom/images/share-fb.jpg)

This game made BoTW a betatest, I admire the programmers and the Nintendo team for building such a nice game, I still don't understand how my switch from 2016 still run this game, AMAZING. I hope other teams like Pokemon could see this game and make a game that is not a shit.

The beginning of the story is amazing, yo meet the demon king and he destroys the master sword, then you almost die and Zelda disapear.

You wake up and then you are in the middle of the sky, you meet the Zonnan and Rauru, the one who gives you the arm with the powers.

The rest of the history is worth playing, I'm not going to explain more about this, just play it cause it deserves every minute you spend on it.


## __What I like__:
- Open world infinite, now with 3 massive maps
- History, better than BoTW
- New Sanctuaries
- New habilites
- Exploration underground
- Everything

## __What I dislike__:
- Maybe Ganondorf is still too easy
- Duplication bugs, I was a bit cheaty
  

## __Overall score__:

10/10

##### Friendly reminder

This is my personal opinion!