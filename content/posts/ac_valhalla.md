---
title: "Assassin's Creed Valhalla"
date: 2023-11-22
tags: ["ps5", "Ubisoft", "action", "adventure"]
---

__Platform__: PS5 \
__Genre__: Action-adventure


Now I understand why people didn't like this Assassins creed game.

![alt](https://www.amd.com/content/dam/amd/en/images/games/assassins-creed-valhalla/530171-assassins-creed-valhalla-key-banner-fade.jpg)

So, this is not an AC, is more like a game that has nothing to be with the AC story. Basically, you are a viking that travels to England in order to build your own story there because Harald conquered all Norway.

The thing is that you and Sigurd are like brothers because when you were a child some viking killed your parents. Then you see in some dreams that you'll betray your brother (the current jarl). 

So the game has 3 main branches, one is to conquer England serving all the terrotories there and having side stories with them (a bit boring and unconnected), another one are your dreams in Valhalla and the third one is to kill the Order.

The main story about conquer Engald in the best one, you meet people and then at the end everyone joins you, what is nice. The war is agains Alfred.

The problem is when everything turned arround and the ending is shitty, finding that you are the reincarnation of Odin, Sigur of Tyr and Basim of Loki, makes no sense.

And morever, the king Alfred is the leader of the order, which makes no sense either.

I'm so dissapointed with this game, hope next one can save the broken dishes.

It took me 73h to finish the game.

## __What I like__:
- The story outside
- Basim
- Environment

## __What I dislike__:
- Really poor ending
- Tooooo long
- Story is not connected
- Boring way to earn PX points
- Decision makes


## __Overall score__:

6.5/10

##### Friendly reminder

This is my personal opinion!
