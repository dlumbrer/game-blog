---
title: "Cult of the Lamb"
date: 2022-12-27
tags: ["switch", "indie", "roguelike", "devolver", "construction/management"]
---

__Platform__: Nintendo Switch \
__Genre__: Roguelike and contruction/management simulation

I'm not a huge fan of rogue-like games, but I saw this game announced on a game event and I decided to give it a try. Now I can say that I'm a pro gamer haha.


![alt](https://i.ytimg.com/vi/6uM7CQGZ6rE/maxresdefault.jpg)

So starting that this indie game is published under the Devolver company, we can say that this game shoul be nice by definition. For the noobs like me, roguelike games are games that you have to repeat in "runs" the same path that you did in order to gain "money/exp/materials" in order to beb stronger and finish the dungeon. So the first thing that came into my mind is that these kind of games are repetitive, but this is not the case.

Cult of the lamb touch perfectly the conbination about roguelike and farming simulation. Basically you have a village where you are the "mesias" with people you will encounter in the dungeon and you have to take care of them, teaching the religion and doing some tasks (feeding, cleaning, etc.)

In order to mantain the village, you have to build buildings for doing different things and keep high the levels of religion, food and cleaning, in the meantime, you have to defeat 4 gods in 4 different levels.

When you finish the 4 bosses you face your god and the challenge is really good. I finished this game in normal difficulty.

A masterpice.



It took me around 15-20 hours to finish it.


## __What I like__:
- Combination of roguelike and construction
- The art
- The environment
- The idea of teaching a religion as game
- The funny things that the game has
- The difficulty level

## __What I dislike__:
- Some bugs that appears due to the first version
- Some hitbox are not that accurate
- I expected more things in the village to improve the powers

## __Overall score__:

9.5/10

##### Friendly reminder

This is my personal opinion!