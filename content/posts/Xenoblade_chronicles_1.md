---
title: "Xenoblade Chronicles: Definitive Edition"
date: 2024-06-30
tags: ["switch", "nintendo", "rpg", "monolith"]
---

__Platform__: Nintendo Switch \
__Genre__: RPG

Finally I could end an RPG from Japan, and of course in my beloved Switch. I started and finished this game in my 2024 research stay in Victoria, Canadaa


![alt](https://assets.nintendo.com/image/upload/c_fill,w_1200/q_auto:best/f_auto/dpr_2.0/ncom/software/switch/70010000029711/d55f4d73ad88beed1ea061aaaacbe772e40f46b598db513f3f85369d3455aa82)


This game starts really really slow, I didn't like how the story begins. But as you are advancing the story, it is getting more deep and everything is makings sense. Firtsly, you think that the machines/mekhons wants to kill people (humas) and you want revenge because they killed (a faced mekhon) your future girlfriend, Fiora. With the Monado, a sword capable of killing mekhon, only mekhon.

The story is really complex, because it starts with the fight of two titans and then it seems that the gods inside the titans were humans playing with a particle accelerator creating new worlds (seriously WTF).

So, it is clear an old game, it comes from the Wii and it is really surprising that a Wii could run this game, the world is vast, and I loved it.

I need some rest now from RPGs I think, but I just realized that there is a DLC included in the definitive edition from switch, sheeeeeeeeeeeeet.

It took me 45 hours to finished it.

## __What I like__:
- Japan artistics
- Open World
- Plenty of missions
- Relaxed mode

## __What I dislike__:
- Some missions are repetitive
- Maybe too long

## __Overall score__:

8.5/10

##### Friendly reminder

This is my personal opinion!