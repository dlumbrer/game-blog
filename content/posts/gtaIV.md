---
title: "GTA IV"
date: 2025-01-02
tags: ["ps3", "rockstar", "action", "adventure"]
---

__Platform__: Playstation 3 \
__Genre__: Accion-Advenure

Finally, the last GTA that I had to finish. And I'm so proud I did play it on a PS3.

![alt](https://assets.nuuvem.com/image/upload/t_banner_big/v1/products/557dbcad69702d0a9c860601/banners/meibrmkk3qm3s3xa4qqv.jpg)

Hype about GTA VI is increasing day by day, and after playing this game, I'm a bit worried about if they are going to produce a woke game.

GTA IV has probably the best story of all the gta's, excluding San Andreas which is amazing.

The story is about Niko Bellic, best character ever, he is balcanic and he is looking for the american dream, but he is an assassin, and it is difficult to change the mind.

Niko works for russians, gangs, friends and also mafia, the game has everything and also you have to choice many times, that will determine your final.

I choose revenge, I killed Francis and Playboy X, all other options about keeping the life or killing, I've chosen kill.


## __What I like__:
- Niko Bellic, most charismatic character in all the gta series
- Physics, much better than GTA V
- Dark story = good story
- Kate dying at the end

## __What I dislike__:
- Final, maybe a bit "harsh"
- Friend calls.

## __Overall score__:

9/10

## __Time spent__:

17 hours and 04 min (Quicker than the avg following how long to beat)

##### Friendly reminder

This is my personal opinion!