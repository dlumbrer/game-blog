---
title: "Nuclear Blaze"
date: 2024-09-22
tags: ["switch", "indie", "platformer"]
---

__Platform__: Nintendo Switch \
__Genre__: Platformer

What a good and short game to finish in a day, it took me only 2.

![alt](https://assets.nintendo.com/image/upload/c_fill,w_1200/q_auto:best/f_auto/dpr_2.0/ncom/software/switch/70010000055375/1c9328436184c53691b5e4d188421c373ee99b34839e6bb38277c65765ef5577)

It gives you what it promises, you are a firefighetr and you have to extinsguish a fire. Then you realize that the fire comes from a nuclear factory where something "interesting" happended.

Basically there are levels in which you have to extinsguish the fire, then you have a final boss that is a chair, something that I don't understand.

It was nice, and the difficulty level is fair enough.

## __What I like__:
- Platformer
- Something fresh and new
- Levels are well designed


## __What I dislike__:
- Maybe too short
- I expected more bosses

## __Overall score__:

8.5/10

##### Friendly reminder

This is my personal opinion!