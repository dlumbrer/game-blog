---
title: "Halo 3"
date: 2023-01-11
tags: ["xbox", "Series S", "gamepass", "microsoft", "shooter", "action", "adventure"]
---

__Platform__: Xbox Series S \
__Genre__: Shooter Action-adventure

Continuing my adventure with the Halo saga, this time I finished the third part, the first one in the Xbox 360.

![alt](https://as01.epimg.net/meristation/imagenes/2020/07/15/avances/1594795396_861858_1594801711_portada_normal.jpg)


So, you continue the history directly when the Halo 2 ends, you go to the ark (arca in spanish) and then yo fight agains the Brutes and Covenant again following the story, bla bla. Basically, you fight with the help of the "emperor" (i don't remember the name) and the goal is to kill the "Truth" cause he wants to activate all the Halo's and destroy everything.

So, the game is good and the story is still strong, but boring. When you are in the ark, the Flood appears and when you have to kill the Truth the flood helps you but when you kill him and stop the Halo's atack, then the flood attacks you again and the "leader" who has Cortana in that moment, talk to you. So, suddenly, a new Halo appears and you decide to activate it. But first you rescue Cortana.

Well, you are about to activate the Halo and the precursor attacks you cause you can destroy the ark with the activation. So more enemies, everything is you enemy (I did not like this part). You activate the Halo, you destroy the ark, you try to scape in the car and you couldn't, but the "emperor" could and in the Earth they cry your dead and more USA things.

Btw, the general and the woman die (which is cool since the story is better).


## __What I like__:
- The saga of Master Chief
- New Halo appears
- How the continue everything
- The end

## __What I dislike__:
- Not as strong as other Halo's
- Cars
- The story is a bit messy
  

## __Overall score__:

7/10

##### Friendly reminder

This is my personal opinion!
