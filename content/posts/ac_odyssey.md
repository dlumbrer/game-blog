---
title: "Assassin's Creed Odyssey"
date: 2022-10-21
tags: ["stadia", "Ubisoft", "cloud", "action", "adventure"]
---

__Platform__: Stadia cloud gaming \
__Genre__: Action-adventure


Probably I'm one of the best assassin's creed fans of the world. I finished all the prevous assassin's and I have them in phisycal even I have a digital PS5 version. For playing this game, I tried Stadia, because I'm in Lugano rigth now doing a stay and the cloud gamming is the best option for me for playing games that are not in the Nintendo Switch. Unfortunately, Stadia is going to close on February 2023, and it's a pity because actually the service is really good and it really surprised me.

![alt](https://gamingbolt.com/wp-content/uploads/2018/07/Assassins-Creed-Odyssey-cover-image.jpg)

I commited a mistake when I started playing this game, I just started (the first time) it when I just finished AC Origins, and Origins was also a long game, so it was a completly mistake to start Odyssey just after it without resting a little about this kind of games. And I bought the game three times, one phisycal for PS4, the PS5 version in digital, and the Staida version. But now is free on PS PLUS, f*** my life.

Let's talk about the game, for me is a masterpiece, my opinion is biased because I'm a huge fan but c'mon, the historic things there are amazing, but I have to say that for me Origin is much better.

Let's get serious, the story of the game is poor and predictable, you are the son of Pitágora (WTF) and the grandson of Leonidas (pffffffhahahaha). Please, it was cooler when you were friend of Da Vinci, but not family. So well, you have to decide if you want to be a man or a woman (Alexios and Kassandra) for the story, that is cool. So the story has a lot of talk actions that will decide the end of the mission, is very simple, don't expect an IA that will change the end of the story in 9320174589236036204712 ways, basycally there are 3 missions that determine the final, but every final is "good", nothing important.

So the story is about two things, find the Kosmos cult and kill them because they try to destroy your family with no reason and the other point is to find 4 artifacts for Pitagoras and close the Atlantis (WTFx2). The only point in common with the other Assassin's is the outside story and the story about the old civilization.

Well, to find and kill the Kosmos is good and enjoyable, but the end is a completly SHIT, you find that the leader is Aspasia and you can get along with her, kiss her or kill her, but the ending is the same.

I'm not going to add more because maybe my mood is so negative today, trust me that this game is totally worth it, I enjoyed from the begining to the end, many hours of fun and discover. Let see what's in Valhalla, because to be hones, I don't like the vikings mythology.

Just two more things to add, Sócrates is a pain in the ass and the boat fights are boooooooring. It took me 49h 55min to finish the game.

## __What I like__:
- The vast land to explore
- Greek environment
- Graphics

## __What I dislike__:
- Sócrates
- Poor story
- Bad ending
- Many useless talks
- Boat figths


## __Overall score__:

8/10

##### Friendly reminder

This is my personal opinion!
