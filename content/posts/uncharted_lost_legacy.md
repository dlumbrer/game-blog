---
title: "Uncharted: The Lost Legacy"
date: 2023-04-20
tags: ["ps5", "psplus", "naughtydog", "action", "adventure"]
---

__Platform__: PS5 \
__Genre__: Action-adventure

The last Uncharted I did not play. Of course, it is far away from the others.

![alt](https://api.time.com/wp-content/uploads/2016/12/uncharted-the-lost-legacy.jpg)

So, I love Uncharted games cause the landscapes that they show are amazing, also some scenes are spectacular and the graphics are quite awesome despite the console limitations.

So basically you are Chloe (first error) in Inida (your country) which is going thorugh a civil war but you are focused on discover Belur and find the horn of Shiva.

Then Asav appears, steal your disk and you run away from him then go to Belur, then discover the city, then defeat Asav and that's it. A series B movie.

One thing that is really cool is that Nadir is there, being now a good character, as well as Sam, that suddenly Asav got him (withou any sense), just fan-made.

A "MEH" game.

It took me 8 hours to finish it.


## __What I like__:
- Landscapes
- Car scenes
- Sam appearence

## __What I dislike__:
- So easy
- The characters don't have charism
- Nothing special
- Boss is shitty
- India has more than what the game shows
- Chloe is not good as a main character


## __Overall score__:

5.9/10

##### Friendly reminder

This is my personal opinion!
