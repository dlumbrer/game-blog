---
title: "Mother Russia Bleeds"
date: 2022-10-26
tags: ["switch", "indie", "beat 'em up"]
---

__Platform__: Nintendo Switch \
__Genre__: Beat 'em up

Another game that I finished during my stay in Lugano, and my first beat 'em up (I mean, in the blog).

![alt](http://gamecloud.net.au/wp-content/uploads/2016/09/mother-russia-bleeds-banner.png)

I'm not a big fan of beat 'em up games, but I decided to start this game because two reasons, the first one is because it is a short game (it took me around 4 hours) and the critics.

The story is about a revolution in Russia (yeah, I know, maybe not the best time to play a game like this), and is simple, there is revolution and you are a gipsy that wants revenge because they experimented with you and gave you a drug (nekro).

It is really cool the art, with bits. The game could be easy or hard, and you a bunch of characters to choose, but the story is the same.

So, was good, just it.


## __What I like__:
- Pistols
- Easy game if you want
- Art


## __What I dislike__:
- Short
- Loading times

## __Overall score__:

8/10

##### Friendly reminder

This is my personal opinion!
