---
title: "Mutant Mudds Deluxe"
date: 2022-04-25
tags: ["switch", "indie", "platformer"]
---

__Platform__: Nintendo Switch \
__Genre__: Platformer

When I saw the title and the firs image of the game my first feeling was that this was a game that the developers did not put effort on it and it will be an easy game in order to earn easy money. I was partialy right.

![alt](https://cdn-ext.fanatical.com/production/product/1280x720/3b501b31-e9f1-4aa8-b836-aec4029258b7.jpeg)

Super Mario look-a-like, but nothing similar. You have three missions on each level, the first one is to finish it, and you get a medal, the second one is to take all the coins, and you will get another medal, the third one is a hidden world that is tipically harder, if you finish it you will earn another medal. In order to unlock the last World (world 5), you have to get two medals (the one for finishing it and the one for getting all the coin) on each level on each world from 1 to 4, for completionist, you have to get all the three medals of each level (of course I didn't do that).

As you earn coins, you can unlock powerups, these power ups are really useful to go throught the levels, there are three and the one that for me is the most useful is the one that double the time with the jetpack. There is also a parallel dimension with ghost mutant mudds that are not killables, so really difficult to pass these levels.

I expected to have more power-ups, different levels and more rewards when finish the levels for the three medal.

There is a second part harder, called Mutant Mudds Super Challenge, I don't want to go throught it now, let see if in the future I will play it.


The game took me around 4 hours to finish it

## __What I like__:
- Easy to play
- Hard levels
- The skill needed in the World 5 worth the game


## __What I dislike__:
- Simple game
- Everything the same
- Looks like a s**t game
- Worlds from 1 to 4 are really easy

## __Overall score__:

7/10

##### Friendly reminder

This is my personal opinion!