---
title: "Assassin's Creed Rogue"
date: 2022-11-07
tags: ["switch", "Ubisoft", "action", "adventure"]
---

__Platform__: Nintendo Switch \
__Genre__: Action-adventure

I'm one of the biggests fans of AC and I forgot to pass this game, I don't remember why I skipped this game, but well, is ok. I finished this game in the last week of my research stay in Lugano.


![alt](https://cdn.akamai.steamstatic.com/steam/apps/311560/capsule_616x353.jpg)

So, finally you see a character that has a dark side, but not as dark as you expect being a templar, becaue you are a former assasins but you decided to become a templar because you want the good for the world, so it does not make much sense.

The mechanics are the same that black flag and the story is a little bit poor, but the good thing is that you meet characters from AC III (the father of Connor) and you finish the game killing the father of Arno.

The outside story is good but meh, nothing special, just another branch of Abstergo and they literally say finally that they are templars.

If you are a AC fan, this is must, if not, play Black Flag instead.


I'm sure that it took me around 10-11 hours to finish it, but I'm not sure about it.

## __What I like__:
- To be a templar
- The story joins Black Flag with AC III
- The finish that is the begining of AC Unity
- Rifles

## __What I dislike__:
- Nothing new
- Really short
- Movements are tricky sometimes
- Poor story
- Boats

## __Overall score__:

7.5/10

##### Friendly reminder

This is my personal opinion!
