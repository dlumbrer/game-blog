---
title: "What the golf?"
date: 2022-05-31
tags: ["switch", "indie", "platformer", "sports"]
---

__Platform__: Nintendo Switch \
__Genre__: Platformer, Sports

I have my heart divided with this game.

![alt](https://fs-prod-cdn.nintendo-europe.com/media/images/10_share_images/games_15/nintendo_switch_download_software_1/H2x1_NSwitchDS_WhatTheGolf.jpg)

Basically, you have to hit the flag with the item you use as a ball in each level. Sometimes is a ball, sometimes is slime, sometimes is a car, even a cow and a human sometimes. Then, you advance in the story and the ball is changing, but the goal is almos the same always, hit the flag. Is a relaxing game that I don't understand, but, try it.


Two hours to finish it, not at 100%.



## __What I like__:
- Winks to indie games
- Funny
- Dynamics for playing


## __What I dislike__:
- Simple
- Sometimes strange

## __Overall score__:

7.5/10

##### Friendly reminder

This is my personal opinion!