---
title: "GTA Vice City - The Definitive Edition"
date: 2022-01-25
tags: ["switch", "rockstar", "adventure"]
---

__Platform__: Nintendo Switch \
__Genre__: Accion-Advenure

Last Christmas I went to the USA, specifically, I stayed in Miami from the 23th to the 26th. I started playing Vice City at the begining of 2022, that means January, so I realized how similar is Vice City to Miami. Maybe you would say, of course is about Miami, you know, Miami Vice, Oceans Drive. But you don't really realize how accurate until you have fresh memories.

![alt](https://i2.wp.com/nosologamer.com/wp-content/uploads/2016/11/gta-vice-city.jpg?fit=660%2C330&ssl=1)

Tommy Vercetti, the story starts a little bit confusing, because it seems that a mob wants to expand to Vice City, and Tommy then is just a soldier but then he makes relationships and earn money, buy things, etc.

The difficulty level is still pretty good, and the end is a challenge in terms of difficulty. The cars, the bikes and the boats are amazing, even better than GTA San Andreas. The map is good but the way they put the two islands with the long distance and the lack of highways sometimes makes the drive tricky.

## __What I like__:
- The amazing music
- The "buy" buildings method
- The way the encourage you to earn money in order to advance with the main story
- The winks to Miami Vice movies (Grandfather of course)
- A hit directly in the nostalgia


## __What I dislike__:
- Again, plenty of BUGSSSSSSSSS
- The story fundamentals, maybe a luttle bit fuzzy
- The end, someone would kill me for this, but I found it casual.

## __Overall score__:

7.5/10

##### Friendly reminder

This is my personal opinion!