---
title: Super Mario 3D World + Bowser’s Fury
date: 2022-01-01
tags: ["switch", "nintendo", "adventure"]
---

__Platform__: Nintendo Switch \
__Genre__: Accion-Advenure



My review and thoughts are focused on the Bowsers Fury expansion, cause the 3D world game I actually finished in my brother's old Wii U.

![alt](https://cdn.videogamesblogger.com/wp-content/uploads/2021/01/Super-Mario-3D-World-Bowsers-Fury-Banner.jpg)

Nintendo, why did you create a game that is f**** amazing but without finishing it, making it shorter? I'm a 2X years old person that grew up playing the Mario's
games, and finally when I found a challenging game like my Super Mario 64, you sell it as an expansion. WHY?. If you like to suffer at some point and not being in colorful song like the Odyssey, that's your game, and if you love cats, even better.


## __What I like__:
- Difficulty improved comparing with "new school" Mario's
- A real "Open world" Mario

## __What I dislike__:
- Why cats? I'm a dog person!
- Very short
- Small world

## __Overall score__:

7/10


##### Friendly reminder

This is my personal opinion!