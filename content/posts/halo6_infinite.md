---
title: "Halo Infinite"
date: 2023-03-23
tags: ["xbox", "Series S", "gamepass", "microsoft", "shooter", "action", "adventure", "open world"]
---

__Platform__: Xbox Series S \
__Genre__: Shooter Action-adventure

I'm sorry to say that this is the worst Halo I've played.

![alt](https://content.halocdn.com/media/Default/community/InfiniteCoverArt/twitterheader_halo_infinite_social_1500x500-e94e24bbf45d4937abacdc0500a3d779.png)


Last Halo game, mixing the campaign with an open world game. Basically, the story is a mess, you met Atriox, who is currently dead (?) and you are in a new Halo governed by the "Desterrados". You lost a fight with Atriox and then a pilot rescue you and both of you land in Halo Zeta.

Then in Halo Zeta you have to discover what is going on, Desterrados wants to control Halo and in the meantime you are discovering that Atriox is dead (due to Halo Wars?) after he killed Cortana. You have a new IA called "Arma" that is helping you and her goal is to kill Cortana or destroy what she did.

A bit messy as I explained. YOu kill the Desterrados and the Emisaria, you found that Cortana has died and the Arma is your new Cortana because is a copy of Cortana.

BAH!

Who are the infinites? Where are the Guardians? IDFK

## __What I like__:
- Open world
- Arma as new IA
- The finish of the arc

## __What I dislike__:
- I don't understand when Cortana died
- Open world is kind of a secondary mission
- Vehicles
- Repetitive
- Desterrados? WTF
  

## __Overall score__:
6/10

##### Friendly reminder

This is my personal opinion!
