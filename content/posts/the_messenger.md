---
title: "The Messenger"
date: 2022-05-30
tags: ["switch", "indie", "adventure", "metroidvania"]
---

__Platform__: Nintendo Switch \
__Genre__: Action-adventure, Metroidvania

If you think that this a ninja game, you are completly wrong.

![alt](https://i.blogs.es/56a89e/220320-messenger-review/1366_2000.jpeg)

If you look for the messenger, you'll see that all the comments are good. This is a 8-bit metroidvainia that starts as a ninja story, and technically, you are a ninja and some of the habilities that you have is from a ninja. And this leads me to one of the best parts of the game, the "cloud jump". When you attack sth on the air, your jump is reset, so they allow you to do another jump.

So, you have to deliver a message crossing all the island where you live that is ruled by demons, when you cross each part of the island you will defeat bosses. SPOILER!: You are not a messenger, the message is a map for the second part of the story.

I'm not going to say more about this game, so is worth playing it.

Don't remember how long did it take me, but I enjoyed every minute.

## __What I like__:
- 8bit/16bit art
- Habilities
- The funny things of the store
- The second part of the story


## __What I dislike__:
- Bosses are "easy" to beat (or maybe I'm finally became a pro legend of gamming)
- Lack of hints in the second 

## __Overall score__:

9.1/10

##### Friendly reminder

This is my personal opinion!