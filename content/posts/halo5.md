---
title: "Halo 5"
date: 2023-02-13
tags: ["xbox", "Series S", "gamepass", "microsoft", "shooter", "action", "adventure"]
---

__Platform__: Xbox Series S \
__Genre__: Shooter Action-adventure

I'm advancing that this Halo impacted me in terms of story and graphics.

![alt](https://c.opencritic.com/images/games/1513/banner.jpg)


Well, this story is a bit messy, the introduce you first the other team of spartans in a mission to defeat the covenant and the first thing that you realize is that the graphics are so awesome, you even can use the zoom as in a normal shooter.

So basically at some point, Master Chief noticed that Cortana is still alive and is like waking some "Guardians" up in order to use "The Manto", so Master Chief tries to find her.

In the meantime, the other spartan time, leadered by Locke are chasing Master Chief because he is doing soem ilegal things looking for Cortana.

You play some missions and the coolest thing is that the Inquisitor appears again and you help him to defeat the covenant, such a nice guy.

I'm not going to add more that that you defeat everything then you save Master Chief, you become a friend of him and Cortana scapes and try to subdue the world with the guardians. The mission finish when Master Chief and Locke return to Lasley.

So, I have to finished only the last one, the Infinite, let see.

## __What I like__:
- New graphics
- Cortana as the enemy
- The "famous" actors as characters
- The return of Master Chief

## __What I dislike__:
- Messy story
- Nothing New
- Now I understand why people started hating Halo
  

## __Overall score__:
7.8/10

##### Friendly reminder

This is my personal opinion!
