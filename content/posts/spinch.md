---
title: "Spinch"
date: 2022-03-30
tags: ["switch", "indie", "platformer"]
---

__Platform__: Nintendo Switch \
__Genre__: Platformer

I'm a lover of indie games, most of the indie games that I played are under the "metroidvaina" genre, but this one is so special.

![alt](https://cdn.akamai.steamstatic.com/steam/apps/794240/capsule_616x353.jpg?t=1637008578)

This game is a platformer, that the main goal that you have is to avoid the "killing" things and rescue your "children" just only running and jumping. I found the gameplay quite similar to the Super Meat Boy and Celeste games, but the psychedelic environment is AMAZING. I remember when I start the game and my first feeling was: "Ok, this game is not for me, plenty of psychedelic colors, I can't even imagin if the people with epylepsy can play this game". But is nice, that's it, you get used to the psychedelic and then you enjoy every level.

And I want to highlight the excellent learning curve that the game has. First of all, the cousin bonus levels were a pain in the ass for me, then in the last worlds, they were the most useful and easy things to do.

And to conclude, I want to say that the [Bossy Boss](https://www.youtube.com/watch?v=WPw6IgUztIk) took me three days of trying (5 minutes per run) until I finished it (with 8 childs and 1 bomb). Ah! And the last level (just before the Bossy Boss) took me 55 minutes to finished. AMAZING!

## __What I like__:
- The excellent learning curve
- The psychedelic environment
- The feeling of success when you finished a long and hard level
- The cute that Spinch is


## __What I dislike__:
- Sometime the FPS went down, specially in the final boss
- Short game

## __Overall score__:

8.5/10

##### Friendly reminder

This is my personal opinion!