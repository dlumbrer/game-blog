---
title: "Into the Breach"
date: 2022-04-10
tags: ["switch", "indie", "strategy"]
---

__Platform__: Nintendo Switch \
__Genre__: Strategy

It has been a while since I played the last strategy game, so I decided to try this one.

![alt](https://upload.wikimedia.org/wikipedia/en/e/e5/Into_the_breach_cover.png)

I have to say that I finished the game in easy mode. I'm not a huge fan of strategy games, but I wanted to try this one because a recomendation of a friend of mine. And i found it a really really good game, easy to learn and play, not too much complex, I mean, you can play without knowing everything but if you want to go deep, you can use complex strategies.

What I found really interesting is the way that the game works, the first feeling that I had playing this game was "KILL ALL THE ALIENS!", and that's not the goal, the goal is to defend the infra of the environment for X turns, then the Aliens will retire and you win, and yes, it is really satisfying to kill the aliens. 

You have to release islands from the aliens, once you are releasing one by one, you can play them in the order you want if you fail in the mission, and if you fail (you died), the game starts again and you can keep only one pilot for the next game (is like multiverses jumps). Once you release 3 islands you can go for the final mission, that you have to defend a bomb and then explodes and you win. But if you go for all the islands you will have more rewards in order to improve your mechas with habilitties and strenght/mov/life.

Really cool this game, I would play it again with other mechas.

## __What I like__:
- Easy to play, don't need to be an expert
- Complex if you want
- A lot of things to customize
- Idea of multiverse when you fail/wil


## __What I dislike__:
- Short game

## __Overall score__:

9/10

##### Friendly reminder

This is my personal opinion!