---
title: "Halo 4"
date: 2023-01-27
tags: ["xbox", "Series S", "gamepass", "microsoft", "shooter", "action", "adventure"]
---

__Platform__: Xbox Series S \
__Genre__: Shooter Action-adventure

Probably the worst halo in terms of story. Now I understand the things that I used to read on internet.

![alt](https://gepig.com/game_cover_460w/6760.jpg)


The first feeling when I started the game was something like "OK, this is a good graphics shooter". They improved comparede to the Halo 3 many things, graphics movements, interactions, etc. I really loved those changes.

So, they rescue you 5 years later and suddenly the covenant is attacking you. I still don't understand why the covenant is attacking you after the peace, and the inquisitor? where is he?, a bit messy everything related to that.

Then the didacta appears (sorry for the spanish, I don't know the name in english), and wants to kill humanity cause we are the virus of the galaxy. Forerunners are kind of gods and they tried to build a perfect thing between human and robots, missing many times building deadly machines and the flood.

You "kill" the didacta and "save" part of the earth, Cortana sacrified herself in the fight and you lost her, anyway she was crazy due to a thing that happens to the IA after many years, still I don't understand.


So, 4/6 Halo's finished, lets see what happens in the next one.



## __What I like__:
- Bette graphics
- The end with the death of cortana.

## __What I dislike__:
- The story a bit messy
- Shorter than the others
- Why el didacta, still I don't understand the lore
  

## __Overall score__:

6/10

##### Friendly reminder

This is my personal opinion!
