---
title: "Huntdown"
date: 2022-04-20
tags: ["switch", "indie", "platformer", "shooter"]
---

__Platform__: Nintendo Switch \
__Genre__: Platformer/Shooter

One of the games that I played a lot when I was a child was the Metal Slug in the bar of my father's village. So I love Run and Gun games.


![alt](https://media.vandal.net/m/39811/huntdown-202051810432842_11.jpg)

I'm a big fan of Arcade games an the 8-bit art. This game includes also the Cyberpunk environment, that incrases its value a lot. The game is just a Run and Gun in a future where the world is devastated, you are a bountyhunter and you have to kill several big boss in 4 "worlds/environments", this environments are places where the gangs are the leaders.

So, you are under the Ms Rose contract and you have to "clean" the city. The mechanics are easy to learn, you yas have a shoot, a jump, a grenade, and a slide. You will found different kind of weapons as you are going into the levels and each weapon is different, from shotguns to laser guns. Pretty amazing the quantity of types.

Each zone has 5 bosses, the last one is the leader of the gang and each one has a different patron of attack, you will fight from robots to samurais, and you have to learn how they attack in order to avoid them and kill them.

You have 3 characters to choose as bountyhunter, each one has a different weapon (mixing between power/speed) and grenade. I chose the robot, but I started with the man for the first zone.

The bad part of the game maybe is that sometimes it becomes repetitive, the story is a little bit poor, and I expected to have a big figth at the end against Ms Rose.


It took me around 5 hours to finish it.


## __What I like__:
- Metal Slug like
- Several types of weapons
- Difficulty of the bosses
- You have to learn the patrons and then fight
- Art of the game
- Cyberpunk


## __What I dislike__:
- Short game
- Poor story
- I expected to fight against Ms Rose

## __Overall score__:

8/10

##### Friendly reminder

This is my personal opinion!