---
title: "Crash Bandicoot 4: It's About Time"
date: 2023-12-03
tags: ["switch", "nintendo", "platformer", "NaughtyDog"]
---

__Platform__: Nintendo Switch \
__Genre__: Platformer


I still remember me in my granmother house playing crash bandicoot 2 with my cousin, those were really good times.


![alt](https://fs-prod-cdn.nintendo-europe.com/media/images/10_share_images/games_15/nintendo_switch_4/H2x1_NSwitch_CrashBandicoot4ItsAboutTime.jpg)

I'm so happy that the team behind Crash games decided to "sell" the character to this new company "Toys for Bob". They took the essence of the first games and they created this wonderful game.

It follows the same platformer strategy of the old games, you have different worlds and levels on them.

It includes new features as the new masks, using common game techniques that fit really well in the game.

What I did not like was to use different characters, not for me.


It took me around 6 hours to finish it.


## __What I like__:
- Nostalgia from the first games
- Funny 
- Return to the origins of this wonderful character
- The high difficulty of some levels

## __What I dislike__:
- Some levels are a bit boring
- Other characters
- Boss are a bit easy

## __Overall score__:

8/10

##### Friendly reminder

This is my personal opinion!