---
title: "Mafia: The definitive edition"
date: 2022-08-19
tags: ["ps4", "2K", "psplus", "action", "adventure"]
---

__Platform__: PS4 - PSPLUS cloud gaming \
__Genre__: Action-adventure


The first Mafia game that I played was the Mafia II game and I don't remember if I played it on a PS3 or directly on PC, but it was a friend's recomendation. Then I tried to play the Mafia III game and I found it boring and nothing to be like the Mafia II game. Now I tried the remastered version of the fisrt Mafie, taking advantage of my ps plus premium suscription using the cloud gaming, during my stay at Lugano (Switzerland).

![alt](https://cdn2.unrealengine.com/Diesel%2Fproductv2%2Fmafia-definitive-edition%2Fhome%2FEGS_MafiaDefinitiveEditionPreOrder_Hangar13_G1A_00-1920x1080-e7457132d8ebeb06c2d663944087c683e4834918.jpg)

You are Tommy Angelo, a person who wants to scape from the mob hands after working for them during many years, the story starts you talking to the police and triying to scape from the city and save you and your family. Tommy was first a taxi driver, single with a simple life and suddenly he met (without wanting it) Pauly and Sam.

The story is f**** amazing, you start in the mob from scratch and then you acomplish mission and you earn the favor of the don. The mission are so nice, with different kind of them (driving, stealth, killing, etc.) and the difficulty level is perfect to fail when you have to fail and win when you have to win.

SPOILERS: What I don't like too much is that I expect the final, I think it is good, but I would have done it more dramatic.


## __What I like__:
- Cars in 30s
- Environment
- Police when you pass the velocity limit
- Story
- The way they join the story with Mafia II (Vito Scaletta)

## __What I dislike__:
- I expected to know more deeply Tommy, like where are his family (parents, brothers/sisters)
- A real Open World like "Go to this icon to start the mission", instead of continue the chapters and the mission and separating the "free mode"

## __Overall score__:

9/10

##### Friendly reminder

This is my personal opinion!