---
title: "The legend of Zelda: Breath of the Wild"
date: 2023-05-15
tags: ["switch", "nintendo", "action", "adventure", "open-world"]
---

__Platform__: Nintendo Switch \
__Genre__: Action adventure


One of the best games in the history of videogames, now I understand and this game for me is the first Zelda that I ever finish.

![alt](https://fs-prod-cdn.nintendo-europe.com/media/images/10_share_images/games_15/wiiu_14/SI_WiiU_TheLegendOfZeldaBreathOfTheWild_image1600w.jpg)

So, you have the entire Hyrule to explore and it is amazing how the phisycs and thisng related to the temperature works in this world.

This review is a bit late, everybody have played this game, but I did not finish it until know, I tap it out.

A masterpice.

It took me around 70 hours to finish it.


## __What I like__:
- Open world infinite
- History
- Puzzles
- Powers
- Exploration things
- kologs
- Everything

## __What I dislike__:
- Maybe Ganon is too easy
  

## __Overall score__:

10/10

##### Friendly reminder

This is my personal opinion!