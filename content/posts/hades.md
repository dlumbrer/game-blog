---
title: "Hades"
date: 2023-01-25
tags: ["switch", "indie", "roguelike"]
---

__Platform__: Nintendo Switch \
__Genre__: Roguelike

Second roguelike game that I finished, and this is another masterpiece.


![alt](https://cdn2.unrealengine.com/hades-banner-1920x1080-1920x1080-019724424.jpg)

So, this is a Roguelike different of Cult of the lamb, but with the same things about repeting runs in different rooms. Basically you are the son of Hades, Zagreo, and you want to escape from hell and meet your real mother (Persephone).

The run is divided in 4 levels with different environemtn and bosses, ant the endof a level you meet a boss and then you go upstairs. The final boss is Hades and is a real pain in the ass.

So you have 3 types of attack and one parry combinated with an attack, usual in roguelikes. You have also different weapons that you will unblock as you are finishing runs and the goal is to farm things in order to improve your power and finally escape.

In the runs you receive blessings from gods that improve something (weapon, attack, etc) and different objects that do the same to you.

It is a good game, but Cult of the lamb was better for me.

## __What I like__:
- The spike weapon
- The environment
- The bosses
- The difficulty
- The feeling when you finish a run, 200 BPM
- The characters you meet, specially Bones

## __What I dislike__:
- Many texts
- Huge path to have the good ending
- Some weapons

## __Overall score__:

9/10

##### Friendly reminder

This is my personal opinion!