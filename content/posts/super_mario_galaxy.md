---
title: "Super Mario Galaxy"
date: 2024-12-25
tags: ["switch", "nintendo", "platformer"]
---

__Platform__: Switch \
__Genre__: Platformer

One of the Mario's games that I didn't finish when I had the chance.


![alt](https://images.launchbox-app.com/8a218c18-5a64-4eb1-9070-4d30e798b249.jpg)

I loved this game, this is exactly what I needed, a platformer, simple and about Mario.

I don't understand why I didn't finish this game when I was a child when I had my Wii.

The game is easy, you have some platforms levels with stars to collect, it surprised me that there are plenty of bosses in all the worlds.

THe story of Stella is sad af.


## __What I like__:
- Levels, and bosses.
- Story.
- Difficulty level.

## __What I dislike__:
- A bit dark.
- Luigi.
- Comets


## __Overall score__:

8.5/10

##### Friendly reminder

This is my personal opinion!
