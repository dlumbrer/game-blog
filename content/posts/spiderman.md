---
title: "Marvel's Spider-Man"
date: 2023-04-10
tags: ["ps5", "ps4", "psplus", "insomaniac", "marvel", "action", "adventure"]
---

__Platform__: PS5 (played PS4 version) \
__Genre__: Action-adventure

I was afraid of this game before playing it, and now I'm so excited to play the Miles Morales one and the new comming.

![alt](https://i.ytimg.com/vi/LTqczRnNqDc/maxresdefault.jpg)

This game has everything I liked, open world in one of my favourite cities, different suits to earn, capability of traveling fast in the city using the buildings, combat easy to learn and with a difficulty curve so f***ng perfect.

I'm not a huge fan of Spider-Man, I'm not into all the lore but I understand who are the main enemies and the main characters (MJ, May, Miles, etc.). The only one that I didn't know was Martin Li, and his story is really really nice.

So basically the game is divided in 3 acts, the first one is about Pikmin (funny this name) and Electro, the second one about Martin Li and the third one about Otto and all the enmies at the same time.

The combine perfectly the secondary missions and achievements with the main story, encouraging the player to find all the backpacks or photo zones, as well as the story is advancing.

The story is a complete masterpiece, i'm not going to talk a lot about it because is always the same regarding OSCORP, Otto, electro. Only the Martin Li one is the new one (at least for me).

It took me ~20 hours to finish it.


## __What I like__:
- Open world New York
- Suits
- Secondary missions
- The amount of different combat modes
- Story
- Photography and backpacks
- How they manage to build a wonderful game

## __What I dislike__:
- Always Spider-Man and his bad luck
- Proably the end, a bit "cold"


## __Overall score__:

9,9/10

##### Friendly reminder

This is my personal opinion!
