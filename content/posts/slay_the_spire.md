---
title: "Slay the Spire"
date: 2022-11-03
tags: ["switch", "indie", "roguelike", "deck-building", "strategy"]
---

__Platform__: Nintendo Switch \
__Genre__: Action-adventure

I'm not a fan of deck-building games, but this one went directly into my heart. I played this game during my research stay in Lugano, just 2 weeks after my farewell.

![alt](https://cdn.akamai.steamstatic.com/steam/apps/646570/capsule_616x353.jpg)

So, I didn't want to play this game at the beginning, but when I was looking for the best indie switch games, everybody agreed that this one is among the best. And my first though was: "Oh, c'mon I don't want to play this deck building game, I don't want to overthink when playing". And I was completly wrong, such an amazing game it is.

So, basically you are a character (you can select between four characters) and you have to hike between fights and challenges, you have three phases and at the end of each phase, you face a big boss. You have cards and power, each card will waste power, when you run out of power or cards, then it's enemy turn.

One of the best things about this game is that are you are advancing in the phase, you will find some role based challenges when you decide your destiny, having benefits and curses.

I really enjoyed it, I finished the game only once, but to have the "real" finish, you have to finish the 3 phases with 3 characters, then you unblock the fourth phase, where the difficulty is considerably higher.


## __What I like__:
- Role based decisions
- Easy to learn
- Adictive
- Four different characters

## __What I dislike__:
- Graphics


## __Overall score__:

9.5/10

##### Friendly reminder

This is my personal opinion!
