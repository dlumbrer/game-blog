---
title: "Paper Mario: the Origami King"
date: 2023-07-10
tags: ["switch", "nintendo", "rpg", "action"]
---

__Platform__: Nintendo Switch \
__Genre__: RPG


My first paper mario, I'm now in this multiverse.

![alt](https://fs-prod-cdn.nintendo-europe.com/media/images/10_share_images/games_15/nintendo_switch_4/H2x1_NSwitch_PaperMarioTheOrigamiKing_image1600w.jpg)

I like that Mario has a set of games like he was in a multiverse, this is my first Paper Mario, the RPG multiverse of Mario.

It is a good game, you fight against origamies, the story is a bit messy but I love the jokes they do with the paper things.

The bosses are quite cool and I loved the combat mode, it mixes perfectly the RPG thing and the puzzle things.

It took me around 25 hours to finish it.


## __What I like__:
- Long game
- Environment
- Combat
- How they play with papers
- Some jokes

## __What I dislike__:
- Ending
- Easy final boss fight
- Maybe a little bit made for children
  

## __Overall score__:

8/10

##### Friendly reminder

This is my personal opinion!