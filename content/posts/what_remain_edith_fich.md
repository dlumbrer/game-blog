---
title: "What Remains of Edith Finch"
date: 2023-12-20
tags: ["switch", "nintendo", "graphicadventure", "adventure"]
---

__Platform__: Nintendo Switch \
__Genre__: Graphic Adventure


I finished this game in a plane from Madrid to Dallas, and it was perfect timing to do it.

![alt](https://cdn2.steamgriddb.com/grid/b5943cfb2b78a2694cc84c22b9381970.png)

So, this story is about a family that for a reason everybody dies. And you are visiting the house of your family and sicovering what happened to them.

Basically each member of the family is a side story where you play with basic phsysic. But the story is worth it. Finally you discover that the protagonist has died, and you are the son of her.


It took me around 4 hours to finish it.


## __What I like__:
- Side stories
- Phisics
- Short game for a flight

## __What I dislike__:
- Maybe to short
- Levels are easy
- Stories could be better


## __Overall score__:

7/10

##### Friendly reminder

This is my personal opinion!