---
title: "Golf Zero"
date: 2022-04-11
tags: ["switch", "indie", "platformer"]
---

__Platform__: Nintendo Switch \
__Genre__: Platformer

Another game that I finished during my 2022 holy week trip.

![alt](https://fs-prod-cdn.nintendo-europe.com/media/images/10_share_images/games_15/nintendo_switch_download_software_1/H2x1_NSwitchDS_GolfZero.jpg)

You like golf and Super Meat Boy? That's your game. An indie game that mixes golf and hability, is like a super meat boy but with the difficulty of playing golf to finish the level, and you only have three balls to do it.

Levels are different and you can have a silver medal or a gold medal once finished it, it depends on the optional goals you achieve, and you don't know what are they until you finish it for the first time, but there are balloons on most of the levels that you have to explode (to ensure the gold medal). You have as well face to face levels, that you have to beat your oponent and insert the ball in the hole before him, and those levels are the trickest but funniest. Unfortunately, there are only like 10 levels of duels facing the the 80 levels that the game has. I found the game a little bit buggy sometimes but is really fun to play. And yes, it is really short, it took me around 2-3 hours to finish it.



## __What I like__:
- The way that they mix golf and hability
- Quick game
- Easy to do a speed-run
- Mixing between duels and hability and speed run levels


## __What I dislike__:
- Some levels are buggy
- Only two worlds

## __Overall score__:

8/10

##### Friendly reminder

This is my personal opinion!