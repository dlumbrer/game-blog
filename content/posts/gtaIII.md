---
title: "GTA III - The Definitive Edition"
date: 2022-01-04
tags: ["switch", "rockstar", "adventure"]
---

__Platform__: Nintendo Switch \
__Genre__: Accion-Advenure

I will always remember when I was a young boy playing with the Eye Toy of the PlayStation 2 and my cousin (older than me) was so excited to play GTA III, I didn't know what was "GTA", and finally, with these "amazind" "bug free" new remastered collection, I played it. 

![alt](https://cdn2.steamgriddb.com/file/sgdb-cdn/grid/e785289359e17762a5385b7f44da20ce.png)

If I'm not wrong, this is the first third person GTA, and it obviously was an inflection point for the games onwards. First of a think that was surprising for me is that the main character does not talk, and the story behind him is a little bit messy, I finished the game but I don't know exactly who was the person who betrayed him at the beginning, why he is a good friend of the first "boss" daughter, but is ok, the way they introduce the character to all the mobs is amazing.

A really difficult end that made me sweat, I won't forget when I finished it, the shout of Victory that I did. But, on the other hand, sometimes the game is not playable, due to the many many many many many many many many many many many bugs that the game has, even more if you play it on a Nintendo Switch.

## __What I like__:
- Main character
- The way the world is opening as you complete missions
- Cars change between islands
- Missions that forces you to know the environment, such as the cars that the gangs use, in order to be a mole.
- The difficulty level.

## __What I dislike__:
- BUGS, BUGS, BUGS, WHY? ROCKSTAR YOU ARE A RICH COMPANY, DO YOUR F**** REMASTERED GAMES.
- Drive mode, sometimes is a pain in the a**.
- The story is not consistent enough.

## __Overall score__:

7/10

##### Friendly reminder

This is my personal opinion!