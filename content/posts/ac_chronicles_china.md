---
title: "Assassin's Creed Chronicles China"
date: 2022-10-25
tags: ["ps4", "psplus", "ubisoft", "cloud", "action", "adventure"]
---

__Platform__: PS4 - PSPLUS cloud gaming \
__Genre__: Action-adventure

Another game that I finished durin my stay in Lugano, I'm glad that I'm again addicted to the Assassin's Creed franchise and I decided that I'm going to finish all the games that I did no try (including the expansions that are standalone games, as Liberation, Freedom Cry or Rogue). Maybe I also play the two games for Nintendo DS, so then I played every single game, let see...

![alt](https://www.powerpyx.com/wp-content/uploads/assassins-creed-chronicles-china-1.jpg)

So, this game is not worthing buying it because it is stupidily short. That doesn't mean is a bad game, but paying for just 5-6 hours... ufff. Anyway, this AC is not as the others, this is a 2D adventure more linke the old Prince o Persia, you have the features of assassin people, hide and so.

The story is good, just a normal story and the best point is the environment, please Ubisoft, make a complete Assassin's creed game in China, I want to explore iiiiiit!

So, funny game but not a deep game. BTW, it is sooo dificult to fight against enemies, and that makes me happy because I was forced to hide and try to assassin them or distract them using the tools. It is not an easy game, and I like it.

## __What I like__:
- 2D dyanmics
- China environment
- Ezio as your master
- Sometimes is a real challenge

## __What I dislike__:
- Really short game
- Not related to the "outside" story
- Expected a bit more probably
- Dumb enemies


## __Overall score__:

8/10

##### Friendly reminder

This is my personal opinion!
