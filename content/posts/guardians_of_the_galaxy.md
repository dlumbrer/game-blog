---
title: "Guardians of the Galaxy"
date: 2022-12-20
tags: ["ps5", "psplus", "square enix", "marvel", "action", "adventure"]
---

__Platform__: PS5 \
__Genre__: Action-adventure

I'm a huge fan of Marvel's Guardians of the Galaxy, they are my favourite heroes and I love their sense of humor and their jokes. This game is a MASTER PIECE.

![alt](https://s2.gaming-cdn.com/images/products/9016/orig/marvel-s-guardians-of-the-galaxy-pc-juego-steam-cover.jpg)

I don't know why this game is not considered as one of the best in the history TBO, this is a game that will touch your heart, and ***** *** if you are not a Marvel fan. Probably this game is not as famous as it deserves because is under Square Enix, and this can be a little bit scary.

So, basically is like watching a really good movie, this game is totally narrative and you follow an story of the guardians of the galaxy, in this case the story behing Adam Warlock and the Church of Truth. I expect to have a movie with this story because it is really really good.

Talking about the environment, 10/10, the scenarios are amazing and you have plenty of planets to visit and interact with many things.

Regarding the gameplay, there is an awesome thing that is the talks, you control Star-Lord and you have to decide the way of the conversations and IDK if this change the fututre, but being able to decide what to say and see what is the result is so ****** awesome!

And more about the gameplay, is a perfect mix about roleplay, powers and puzzles. For me, the perfect MIX.


I can talk about many other awesome things, but I would like you to try it.

## __What I like__:
- Guardians of the Galaxy
- Combat gameplay
- Sense of humor
- Environment
- Story
- Ending
- Starting
- EVERYTHING

## __What I dislike__:
- I want more now T.T


## __Overall score__:

10/10

##### Friendly reminder

This is my personal opinion!
