---
title: "Kirby and the Forgotten Land"
date: 2022-04-17
tags: ["switch", "nintendo", "platformer"]
---

__Platform__: Nintendo Switch \
__Genre__: Platformer

One of my favourite game characters (with Sonic), my lovely pink ball Kirby!

![alt](https://cl.buscafs.com/www.levelup.com/public/uploads/images/733100/733100.jpg)

If you expected (as I expected) and Open World game, let me come you down from the clouds and I'll tell you that this is another platform Kirby game, with all the things that all the other Kirby games have. The only two things that this game adds are the 3D levels, you can move in all the axis, and the Kirby transformation in common things.

Let's start with the 3D levels: OK, you can explore more than in 2D, but come on, the took your hand and the limitation to explore is there. Regarding the transofrmation things, they are limited to the primitives of squares, circles, triangles and a car. They are funny to use, but I would go to more complex things, not showing that this game is another Nintendo game made it for kids. 

The difficulty level is a joke, you can put the "SAVAGE" mode at start (I put it) and I didn't die in the entire main story, I died just in the Land of Dreams (once the main story is finished).

Let's go for the good things, you can evolve the kirby powers at least 3 times, finding the plans of the habilites in the levels.

The goal is to save Waddle Dees from the forgotten animals, you old enemies, WTF. I found strange the story, I did not understand the meaning of the characters.

And it took me like 10-12 hours to finish it. I finished the main story on 13th.


## __What I like__:
- Finnaly a Kirby 3D game that really is cool on Switch
- The way that they used the common things that Kirby can abosrb and transform
- Nostalgia hit
- 3D levels
- The difficulty levels of the lands of dreams ("Valle de los sueños" in spanish)
- Evolving powers

## __What I dislike__:
- Pretty short
- Pretty easy the main story
- Few thing to transform into
- I expected an Open World
- The challenge levels, I don't know why they put them, completly useless

## __Overall score__:

7.777777/10

##### Friendly reminder

This is my personal opinion!