---
title: "Sonic CD (Sonic Origins)"
date: 2023-07-28
tags: ["switch", "sega", "sega cd", "platformer"]
---

__Platform__: Nintendo Switch - Sega CD \
__Genre__: Platformer

I continue the saga with the second game of Sonic, Sonic CD, the first and unique one in the Sega CD console.

![alt](https://i.blogs.es/b82b0e/soniccd/1366_2000.jpg)

So the story is the same, yo control Sonic and have to defeat eggman, the only difference is that metal sonic appears, but is too easy to defeat him, you just need to win them in a race. Also Sonic's girlfriend appears in this game you need to save them from Eggman.


## __What I like__:
- Nostalgia
- Sonic always will be my favourite videogame character
- The idea about the future and past
- New scenarios


## __What I dislike__:
- Eggman is easier than the first game
  

## __Overall score__:

8/10

##### Friendly reminder

This is my personal opinion!