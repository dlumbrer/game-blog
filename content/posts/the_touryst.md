---
title: "The Touryst"
date: 2022-05-02
tags: ["switch", "indie", "adventure", "puzzle"]
---

__Platform__: Nintendo Switch \
__Genre__: Action-adventure, Puzzle

If you are in the mood of chill and play a game that is not difficult, is beatiful and easy, this is your game.

![alt](https://fs-prod-cdn.nintendo-europe.com/media/images/10_share_images/games_15/nintendo_switch_download_software_1/H2x1_NSwitchDS_TheTouryst_image1600w.jpg)

A really artwork game, my congratulations to Shin'en for developing this game. Please, take into account that this is a short game, that you cannot play again, because it is a simple story that has a beginning and an end.

Basically, you are a touryst in an island and you find a monument, you enter the monument and resolve a puzzle, once you finish this puzzle, an old man appears and requires you to get 4 monument cores for something special that he is not going to explain you. These cores are in different islands, and you need first a flyer of the island before going there, is like a passport, when you have the flyer, you go to the boat captain and the he takes you to the island.

In each island you can talk to people and some of them will give you a mission to do, that is gonna be noted into the todo list that you have. The best part of this is the missions are so funny, there are missions from navigate with a boat to drive a drone. Amazing!

And if you like pixel art and the isometric camera, you are gonna love it more if possible (although sometimes the camera movement is tricky in some puzzles)

The puzzles that are in the monuments/temple are short, and sometimes you can find them easy, but the mechanics to solve them are so fresh.

The game took me around 3 hours to finish it

## __What I like__:
- The chill environment
- The camera
- Minigames in the TO-DO missions
- The names of the islands
- The little secrets that the game has (like the dance things)
- The camera


## __What I dislike__:
- Easy puzzles
- Short game

## __Overall score__:

9/10

##### Friendly reminder

This is my personal opinion!