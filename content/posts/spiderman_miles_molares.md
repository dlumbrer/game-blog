---
title: "Marvel's Spider-Man: Miles Morales"
date: 2023-05-02
tags: ["ps5", "psplus", "insomaniac", "marvel", "action", "adventure"]
---

__Platform__: PS5 \
__Genre__: Action-adventure

This game (the story) continues just after the ending of the Spider-Man game.

![alt](https://image.api.playstation.com/vulcan/ap/rnd/202008/1420/HcLcfeQBXd2RiQaCeWQDCIFN.jpg)

So, Peter has a trip to Europe (or any other place, I don't remember it right now) and he had to let New York in the Miles Morales hands, the new young Spider-Man. I found this a bit forced but it is clever.

Mile's Mom is into politics, against Roxxon. Your friend Phin is there as well and you find that is Tinkerer (don't know if this enemy is into the marvel universe TBH), Tinterker born cause his brother died because of the NuForm, very toxic.

In the meantime, you meet your aunt and also you find that is another enemy, but he became your friend and in the end even helped you.

Summarizing, worth playing and spend hours flying through the NY streets.


## __What I like__:
- Open world New York, winter edition
- Miles Morales power
- Probably one of the best Open World games.
- The ending
- Everything the inherited from the first game

## __What I dislike__:
- The new face of Peter Parker
- So short, it should be a DLC of the first game.


## __Overall score__:

9,5/10

##### Friendly reminder

This is my personal opinion!
