---
title: "New Super Mario Land"
date: 2024-08-14
tags: ["snes", "psvita", "Nintendo", "platformer"]
---

__Platform__: Super Nintendo Entertainment System \
__Genre__: Platformer


Another game that I finished during my research stay of 2024 in Victoria, Canada. Played on a PSVita with the Retroarch SNES emulator.

![alt](https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEj8Qg20ogfqwhWgvE6ofCkoqMPJRgxWPYIymRH_y4RkInOc9Xtg1_A50aCS67t5HL4F178uPn8yUKt-VPxM5oIwVFtUrD-HUDx9Jx2WA3hD3Gvbz7MyPOhhiXOfq0Xb59OBMQWljN0YoK8/s1600/Annotation+2019-11-22+160713.png)

There is not much to say about this game, one hour long (at most), first platformer of Mario but adapted to the SNES following the "NEW" Mario Games.

I love that this is a port made for the SNES, which means that is developed for the SNES.

Amazing, and worth playing.


## __What I like__:
- To be able to play again one of my first games played
- It is short
- Mario always is a win-win


## __What I dislike__:
- That is not a Nintendo Game
- Some sprites are messy
- Easier than original


## __Overall score__:

8/10

##### Friendly reminder

This is my personal opinion!