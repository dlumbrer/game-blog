---
title: "Super Mario Bros. Wonder"
date: 2023-11-23
tags: ["switch", "nintendo", "platformer"]
---

__Platform__: Nintendo Switch \
__Genre__: Platformer


Probably one of the best 2D platforms I've ever played, and for sure, the best Mario Bros ever.


![alt](https://fs-prod-cdn.nintendo-europe.com/media/images/10_share_images/games_15/nintendo_switch_4/2x1_NSwitch_SuperMarioBrosWonder.jpg)


It is amaizing how a game like this could be the GOTY.

I don't have too much to say than the art, level desing and ideas are the best ones I've ever seen.

Worth playing game.


It took me around 7 hours to finish it.


## __What I like__:
- Every level is amazing
- Art
- Desing
- Wonder flowers
- Elephant power

## __What I dislike__:
- Maybe too short

## __Overall score__:

9.8/10

##### Friendly reminder

This is my personal opinion!