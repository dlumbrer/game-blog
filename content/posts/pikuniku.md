---
title: "Pikuniku"
date: 2023-02-05
tags: ["switch", "indie", "platformer"]
---

__Platform__: Nintendo Switch \
__Genre__: Roguelike

An small and simpel game that I finished in my trip to FOSDEM 2023

![alt](https://assets.nintendo.com/image/upload/c_fill,w_1200/q_auto:best/f_auto/dpr_2.0/ncom/en_US/games/switch/p/pikuniku-switch/hero)

So I decided to donwload this game preparing my trip to Costa Rica because I was looking for short games in order to play them in the flight. The game is like that, short, but the probleman y that the game is so easy and is not challenging enough.

I expected much more, even the final boss is stupidly easy.

Summarizing, you are a beast that suddenly appears from a mountain in a village and you break a bridge and then you decide to fix it in order to let people now that you are not bad. In the meaintime, there is a company that gives free money in exannge of resources. Then the people from the village and the other worlds do not like the robots and the company (Shunshine INC) steal the resources and the decide to saboteur them. You pursuit the robots and you see that the real plan of the boss is to remove all the villages and built a perfect world with robots.

You stop the boss and the robots, easy.

There are minigames that are curious, but easy.


It took me around 4-5 hours to finish it.

## __What I like__:
- Characters
- Worlds
- Some jokes

## __What I dislike__:
- Not challenging
- Very simple
- Small story
- Does not add nothing important to your life

## __Overall score__:

6/10

##### Friendly reminder

This is my personal opinion!