---
title: "Mario Golf"
date: 2024-08-12
tags: ["gbc", "psvita", "Nintendo", "Camelot", "sports"]
---

__Platform__: Game Boy Color \
__Genre__: Sports


Another game that I finished during my research stay of 2024 in Victoria, Canada. Played on a PSVita with the GBC emulator.

![alt](https://mario.wiki.gallery/images/thumb/e/ed/MarioGolfGBCBoxArtCover.jpg/1200px-MarioGolfGBCBoxArtCover.jpg)

What can I say from Camelot, they did an awesome job with Mario Tenis and Mario Golf, they mix perfectly the role playing game with the sports, with an intuitive story and gameplay.

I really miss this company and games like this, it's a pity that in newer Mario Golf/Tenis games they are not including story mode.

The story is not that complicated, is the same than Mario tenis, but I love how you are playing a game about golf and when you accomplish all the tournaments, you discover the Mario place and you face the Mario characters.

Maybe I liked more the Tenis game than the Golf, but that's because I always prefered Tenis to Golf. Anyway, a good and hard game!

## __What I like__:
- How Camelot mixes roleplaying with sports
- The second ending
- Mario characters


## __What I dislike__:
- Shorter than Mario Tennis
- The difficulty (so fucking high)


## __Overall score__:

8.5/10

##### Friendly reminder

This is my personal opinion!