---
title: "Sonic the Hedgehog (Sonic Origins)"
date: 2023-07-25
tags: ["switch", "sega", "megadrive", "platformer"]
---

__Platform__: Nintendo Switch - Sega Megadrive \
__Genre__: Platformer

One of my first games I played.

![alt](https://i.ytimg.com/vi/Gsa_4s3CjmI/maxresdefault.jpg)

I still remember when I was young and I used to go to my father's village to enjoy the summer there. I used to play with my cousing the Sega megadrive that he had. One of the games that he had was Sonic the Hedgehog, and it was love at first sight. There were more games but his one was my favourite.

Nostalgia just kicked it in.


## __What I like__:
- Nostalgia
- Sonic always will be my favourite videogame character
- Everything around it


## __What I dislike__:
- A bit short
  

## __Overall score__:

9.5/10

##### Friendly reminder

This is my personal opinion!