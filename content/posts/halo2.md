---
title: "Halo 2"
date: 2022-11-29
tags: ["xbox", "Series S", "gamepass", "microsoft", "shooter", "action", "adventure"]
---

__Platform__: Xbox Series S \
__Genre__: Shooter Action-adventure

So I finally decided to buy the xbox series S and take advantage of the game pass, and I started playing the second part of Halo, and it was really good.

![alt](https://i.blogs.es/6003d3/halo-2/1366_2000.jpeg)


I made a mistake and I looked for the puntuation of the game after finishing it, and it surprised me because it has more points that the first verions of Halo, and yeah, it is a good game, but the story of the first one is better IMO.

So, you starts just after the ending of the first Halo, and you find that there is another Halo and the Covenant wants to use it as a tool for the "Great trip", like a sect.

Then the story is simple, you kill people from the Flood, people from the Covenant, you find the icon and avoid the execution of Halo.

What is good of this game is that there are two stories in paraller, one with Master Chief and one with a person from the Covenant that was betrayed and try to salve the people from them.

I don't want to add more to this story, is worth play it, try it if you can.


## __What I like__:
- The story
- Being able to play as an Elite
- How it ends 

## __What I dislike__:
- Not many cars/spaceships
- Short
- Repetitive sometimes

## __Overall score__:

8.8/10

##### Friendly reminder

This is my personal opinion!
