---
title: "GTA San Andreas - The Definitive Edition"
date: 2022-03-23
tags: ["switch", "rockstar", "adventure"]
---

__Platform__: Nintendo Switch \
__Genre__: Accion-Advenure

The jewel of the crown of the GTA games, the master piece of Rockstar. Another GTA that I could no finish when I was a child and today I enjoyed and I'm impressed about the features that it has, even more cool things that the last GTA V.

![alt](https://i.ytimg.com/vi/H4rYVsJ4v9Y/maxresdefault.jpg)

The CJ story is one of the most curated stories of all GTA's, it is like the Forest Gump story, CJ did and worked for everything, gangs, police, garage, tech stores, auto stores, casino, spy, music producer and so on. And all the story is strong, what means that you can follow it getting into it in the right way. The game also has feautres that I found amazing, like the gang neighborhood conquer or the jetpack for instance, why Rockstar did not continue with these things?

But there is the worst part, the bugs, again, why Rockstar relied on a company that cannot do a good remaster. I crashed the game at least two times per play, completly useles, the game is laggy sometimes, and the textures sometimes did not charge well, producing "stops" during the gameplay.

The very last thing: I almost broke my Switch trying to pass the [A Home in the Hills](https://gta.fandom.com/es/wiki/A_Home_in_the_Hills) mission. OMFG, what a difficult mission, it took me 3 days, I had to cancel the mission in the middle in order to have more life, to find rocket launchers, to do it perfect. What a pain in the ass, and I think that I acomplished the mission using a bug (FACEPALM). But when I finished the mission, it was like a hard exam that you passed with a 5.0.

## __What I like__:
- CJ story
- Huge map with winks to Los Angeles, San Francisco and Las Vegas (one of the tours that I did in USA)
- Different vehicles
- Gang conquer feature
- Casino playable
- EVERYTHING


## __What I dislike__:
- Switch version = BUGS
- A Home in the Hills

## __Overall score__:

9/10

##### Friendly reminder

This is my personal opinion!