---
title: "Halo: Combat Evolved"
date: 2022-11-14
tags: ["xbox", "gamepass", "cloud", "microsoft", "shooter", "action", "adventure"]
---

__Platform__: Xbox (XCloud game pass) \
__Genre__: Shooter Action-adventure

How did I miss this game! I was because I'm a Sony person, but with this new game pass and the possibility of playing in cloud, finally I tried it. I played and finished this game during my stay in Lugano, just before it ended.


![alt](https://cdn.wccftech.com/wp-content/uploads/2020/01/halo-combat-evolved-pc-flight-delay.jpg)


I'm so late to enjoy this game, It's a pity because the game is absolutely amazing. So, first of all, this an old shooter, from the first XBOX version, so forget about the normal controllers unless you are playing the anniversary edition with the possibility of putting the new controllers for shooting, crouch and so.

This game is difficult, I'm not proud but I had to put the game on easy mode cause sometimes is a really big deal.

So basically you are in war with the cult (El culto en español) and you have to kill them, then in the middle of the space war you are forced to land into the "HALO", which is like a planet in a disk form, quite weird to be honest.

So basically you continue the war there and you suspect that the HALO is a weapon and then you have to find where is the control room for controlling it, then you realize that is a tool from the precursors, and the plot twist is that in the HALO there are "The flood" that are like zombies that eat any kind of life.

So the police of the HALO (the balls) wants to destroy every life including the flood for solving the problem, then you stop them and destroy the halo, ending with all the life inside.

In the middle there is your captain that dies in a part of the story being a part of the flood.

This game took me 10 hours or so to finish it.

## __What I like__:
- The story
- The plot twist
- The difficulty level
- To finally understand whats behind the halo community

## __What I dislike__:
- Sometimes repetitive (I know that is an old game, I expect so much from the second part)
- THE CAR CONTROLS

## __Overall score__:

9/10

##### Friendly reminder

This is my personal opinion!
