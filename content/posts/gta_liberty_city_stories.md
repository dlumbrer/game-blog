---
title: "GTA Liberty City Stories"
date: 2024-07-08
tags: ["psp", "psvita", "Rockstar", "action", "adventure"]
---

__Platform__: PSP \
__Genre__: Action-adventure


Another game that I finished during my research stay of 2024 in Victoria, Canada. Played on a PSVita with the PSP emulator.

![alt](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTX3zp3WWOy7mE75naQJNAI2BVwGx6CVWL8sA&s)

I was in the journey of completing all the gtas, and now it was the turn of this game that I used to play when I was a child but never finished. This is a side story of GTA III, I think that the events of this game happen before, as you are Toni, of the guys of Salvatore, and I think you kill Salvatore in GTA III.

The game is fun because the map is the the one from GTA III and it follows the same structure, missions to unlock people/more map and funny stuff, including races, stealing, killing. You are in a mob, so this is your work.

I think it took me around 10-11 hours to finish it.

## __What I like__:
- The difficult level
- Many cars and bikes
- Story about Salvatore, well connected 

## __What I dislike__:
- Story isn't deep
- Short, but it was released on a PSP, which is nice


## __Overall score__:

8/10

##### Friendly reminder

This is my personal opinion!
