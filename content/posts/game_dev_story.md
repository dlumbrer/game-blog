---
title: "Game Dev Story"
date: 2023-12-25
tags: ["indie", "switch", "construction/management", "simulation"]
---

__Platform__: Nintendo Switch \
__Genre__: Contruction/management simulation

So this is probably the first game of construction/management simulation that I ever finished.

![alt](https://www.speedrun.com/static/theme/68qmq2rp/banner.jpg)

I wanted to play a pixel game, and I decided to try this one because of the story behind, about running a dev company story.

This game is good, but easy. I guess that it includes basic mechanics of this type of games, but as it was my first game, I enjoyed it lot.

You have to manage new games, contracts, consoles and people, everything. It is a short game, perfect for flights.

I finished this game during my Christmas 2023 trip to USA.


It took me around 6 hours to finish it.


## __What I like__:
- Easy to play
- Way to manage resources
- Funny names and references
- I was completly immersed

## __What I dislike__:
- Maybe too easy
- Some bugs with the text
- Bad translations


## __Overall score__:

8/10

##### Friendly reminder

This is my personal opinion!