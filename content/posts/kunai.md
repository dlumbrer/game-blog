---
title: "Kunai"
date: 2022-05-17
tags: ["switch", "indie", "metroidvania", "adventure"]
---

__Platform__: Nintendo Switch \
__Genre__: Metroidvania, Action-adventure


A good surprise I had with this game, really nice, beautiful and good gameplay, don't know why is not even more famous.

![alt](https://cdn.akamai.steamstatic.com/steam/apps/1001800/capsule_616x353.jpg?t=1651742347)

You are a tablet that woke up in a world that are dominated by red computers with weapons, there are a group of blue computers and robots that are the resistance and you will help them among the story.

There is a point that I don't understand yet, and is the story, let me recapitulate the steps that you do in the story:

1. Wake up in a lab and find the Katana, then you find the Kunai in a temple
2. Go to the resitance, find the boss of the resistance and he will guide you
3. Go to another temple and find the suriken to open doors
4. Then you find a huge MECHA that needs a core to wake up (I thought that this was the final boss)
5. Suddenly you go to a floating ship and find the boss that is like a pirate but I don't understand his role, because in the end he appears but you don't fight to him.
6. You discover a mine that is only a way to go to the city, previous defeating a boss in a temple that will give you the big weapong (bazooka)
7. Find the city and the bad guys are the police (LOL)
8. More things..... and probably others that I forgot
9. You wake up the mecha and you fly to a new planet
10. A virus is introduced to you DON'T KNOW WHEN.
11. You defeat the virus
12. You defeat the final boss
13. You return to the first world and that's it

So, a complete messy, but the gameplay es awesome, clean, cool and many weapons that you can improve in a store, my 10 of this. In each temple there is a boss with different mechanics, they are not easy but not hard, the perfect point to enjoy and not having a frustation (at least regarding my level).

Also, I'm a geek, so having a story that face old computers to new ones gives extra points ;)

## __What I like__:
- The art
- The weapons
- The Kunai for moving like Tarzán
- The way they change the colors between zones
- Computers world and their winks
- AMAZING gameplay

## __What I dislike__:
- Maybe a little bit short
- The finish and the messy story


## __Overall score__:

9/10

##### Friendly reminder

This is my personal opinion!