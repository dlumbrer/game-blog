---
title: "WarioWare: Get It Together!"
date: 2022-01-28
tags: ["switch", "nintendo", "action"]
---

__Platform__: Nintendo Switch \
__Genre__: Action

I've always been a fan of Wario, and more of the WarioWare games, I started playing them from the GBA game, and I found it from the very begining a playable game that brings out the main features of each console. 

![alt](https://fs-prod-cdn.nintendo-europe.com/media/images/10_share_images/games_15/nintendo_switch_4/H2x1_NSwitch_WarioWareGetItTogether_image1600w.jpg)

Summary of the WarioWare that I remember that I played and the main characteristics of the minigames:

1. WarioWare of GBA: you use A, B, R, L and Arrows
2. WarioWare of DS: you use the pencil and the touch screen and the mic
3. WarioWare of Wii: you use the wiimote features
4. WarioWare of WiiU: you use the screen and the TV
5. WarioWare of Switch: you use buttons = "Hi, I'm nintendo, I want your 50$ for this game that doesn't add something new"

FACEPALM.

I'm not going to talk about the story, because I really love the "fun" Wario, with the garlic, the stinky way, the jokes. And this "GAME" use the metaphor of programming that HAS NO SENSE in a fun GAME. Crappy and simple scenarios, too many characters that more than a half are completly useless.


## __What I like__:
- Finally a new WarioWare game
- Hours fun
- All the "main" characters of WarioWare in a game = nostalgia


## __What I dislike__:
- Short, very short
- Many characters that have similar characteristics
- Doesn't bring out the characteristics of the Switch, it uses characteristics of the GBA.
- Boring and few minigames
- Games mode that have no sense, like the tower one.ç
- Not a funny story, is more like a BORING story
- Where is the ***** garlic?

## __Overall score__:

6/10

##### Friendly reminder

This is my personal opinion!