---
title: "GTA Vice City Stories"
date: 2024-08-01
tags: ["psp", "psvita", "Rockstar", "action", "adventure"]
---

__Platform__: PSP \
__Genre__: Action-adventure


Another game that I finished during my research stay of 2024 in Victoria, Canada. Played on a PSVita with the PSP emulator Adrenaline. I'm on the quest of finishing all the 3D GTA, I left only GTA IV!.

![alt](https://upload.wikimedia.org/wikipedia/en/thumb/3/3e/GTA_Vice_City_Stories_PSP_boxart.jpg/220px-GTA_Vice_City_Stories_PSP_boxart.jpg)

I cannot believe that this game was on the PSP, it is a really good game with many features, it is much better than its predecessor GTA Liberty City Stories.

They did a jump on quality on this game, they have plenty of good missions, vehicles and a good story.

Well the story is a bit messy because first you want to earn money for your brother who is sick, but it ended being a dealer, human-traficking person.

It has plenty of new vehicles including helicopters, which I like the most.

## __What I like__:
- New vehicles
- Missions
- Vice City
- Some missions that were different of the other 3d GTA games.

## __What I dislike__:
- Long trips on missions
- The lack of a highway


## __Overall score__:

9/10

##### Friendly reminder

This is my personal opinion!
